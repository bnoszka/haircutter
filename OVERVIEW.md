```
git clone https://github.com/symfony/skeleton.git -b 4.3 _Haircut
```

```
composer install
```

```
composer req api
```




```
sc doc:database:cr
```

alias `sc` stands for symfony php bin/console - https://symfony.com/cloud/


```
sf proxy:domain:attach haircut.pl
```

```
sc doc:sc:up --force
```

```
composer require nesbot/carbon
```


Co dalej?

1. Dopracowanie endpointu zwrotu wszystkich slotów od 08:00 do 20:00 w postaci kolekcji slotów
z ID, STARTS_AT, ENDS_AT oraz z przypisaną vizytą, jeśli takowa istniej. 

   Sloty nie są zapisawane w bazie, natomiast są tworzone w locie w endpoincie oraz z pomocną klasą (np, Ranger), 
która generuje tablicę okresów 30-minutowych z obiektami datetime. Później w klasie VisitManager
tworzymy metodę, np getSlotsWithVisits, wstrzykujemy Ranger'a, pobieramy visity z danego dnia, oraz pętlą w pętli przypisujemy wizyty do danego slotu.

   Tak powiniśmy zwrócić listę slotów z danego dnia.

   Nie udało się, ponieważ już nie pamiętam dlaczego otrzymuje błąd z API platform: Unable to generate an IRI for \u0022App\\Model\\Slot\u0022.

   Chociaz mając więcej czasu pewnie bym doszedł, bo wiem, że miałem już taki problem.

2. Dla tworzenia nowych/edycji wizyt nie dawałbym startsAt i endsAt. Stworzyłbym endpointa, ktora również generuje godziny: 08:00, 08:30, 09:00, itd, aby łatwo było je wyświetlić na froncie (przydatne dla startTime, endTime).

   Czyli model VisitInput to. id, date, startTime, endTime, seat. 
   
   W tym celu skorzystałbym z tego: https://api-platform.com/docs/core/dto/#specifying-an-input-or-an-output-data-representation 
 
3. Również przydatnym endpointem mógłby być /calendar/{month}, również do łatwiejszej reprezentacji wizyt w ciągu miesiąca.

Niestety poległem, na API platform: Unable to generate an IRI for \u0022App\Model\Slot\u0022.

4. Walidacja wizyt, działa poprawnie.


```
POST /visits HTTP/1.1
Content-Type: application/json; charset=utf-8
Host: haircut.pl.wip
Connection: close
User-Agent: Paw/3.1.8 (Macintosh; OS X/10.14.6) GCDHTTPRequest
Content-Length: 50

{"endsAt":"2019-11-01 12:30:00","seat":"/seats/1"}
```

```
{
  "@context": "\/contexts\/ConstraintViolationList",
  "@type": "ConstraintViolationList",
  "hydra:title": "An error occurred",
  "hydra:description": "seat: This period is occupied on that seat",
  "violations": [
    {
      "propertyPath": "seat",
      "message": "This period is occupied on that seat"
    }
  ]
}
```
