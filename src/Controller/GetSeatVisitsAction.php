<?php

namespace App\Controller;

use App\Entity\Seat;
use App\Manager\VisitManager;
use Symfony\Component\HttpFoundation\Request;

final class GetSeatVisitsAction
{
    private $visitManager;

    public function __construct(VisitManager $visitManager)
    {
        $this->visitManager = $visitManager;
    }

    public function __invoke(Request $request, Seat $data)
    {
        return $this->visitManager->findTodaysVisits();
    }

}