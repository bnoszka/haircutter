<?php

namespace App\Controller;

use App\Manager\VisitManager;
use Symfony\Component\HttpFoundation\Request;

final class GetDaySlotsAction
{
    private $visitManager;

    public function __construct(VisitManager $visitManager)
    {
        $this->visitManager = $visitManager;
    }

    public function __invoke(Request $request, $data, $id)
    {
        return $data;
    }

}