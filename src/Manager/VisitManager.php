<?php

namespace App\Manager;

use App\Entity\Visit;
use Carbon\Carbon;

class VisitManager extends AbstractManager
{
    protected $class = Visit::class;

    public function findVisitsBetweenDatesBySeat(Visit $visit)
    {
        return $this->repository->findVisitsBetweenDatesBySeatQueryBuilder($visit->getStartsAt(), $visit->getEndsAt(), $visit )->getQuery()->getResult();
    }

    public function findTodaysVisits()
    {
        $starts = Carbon::now()->setTime(8,0,0);
        $ends = Carbon::now()->setTime(20,0,0);

        return $this->repository->findVisitsBetweenDatesBySeatQueryBuilder($starts, $ends )->getQuery()->getResult();
    }
}