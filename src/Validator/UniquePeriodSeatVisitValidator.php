<?php

namespace App\Validator;

use App\Entity\Visit;
use App\Manager\VisitManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniquePeriodSeatVisitValidator extends ConstraintValidator
{
    private $visitManager;

    public function __construct(VisitManager $visitManager)
    {
        $this->visitManager = $visitManager;
    }

    public function validate($visit, Constraint $constraint)
    {
        if( ! $visit instanceof Visit)
        {
            return;
        }

        if ( ! $visit->getStartsAt() instanceof \DateTime || ! $visit->getEndsAt() instanceof \DateTime )
        {
            throw new \UnexpectedValueException($visit, 'DateTime');
        }

        $takenVisits = $this->visitManager->findVisitsBetweenDatesBySeat($visit);

        if( count( $takenVisits ) > 0 )
        {
            $this->context
                ->buildViolation($constraint->message)
                ->atPath('seat')
                ->addViolation();

        }
    }
}