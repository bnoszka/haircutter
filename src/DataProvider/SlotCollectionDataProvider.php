<?php

namespace App\DataProvider;

use ApiPlatform\Core\DataProvider\CollectionDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use App\Model\Slot;
use Symfony\Component\Serializer\SerializerInterface;

final class SlotCollectionDataProvider implements CollectionDataProviderInterface, RestrictedDataProviderInterface
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Slot::class === $resourceClass;
    }

    public function getCollection(string $resourceClass, string $operationName = null)
    {
        $slots = [
            new Slot(1, '3', '3'),
            new Slot(2, '3', '3')
        ];

        $slots = $this->serializer->normalize($slots, null, ['groups' => ["out"]] );

        return $slots;


    }
}