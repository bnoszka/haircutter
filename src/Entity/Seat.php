<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\GetSeatVisitsAction;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "get"={
 *          },
 *          "post"={
 *          },
 *     },
 *     itemOperations={
 *          "get"={
 *          },
 *          "put" = {
 *          },
 *          "get_slots"={
 *              "method"="GET",
 *              "path"="/seats/{id}/visits",
 *              "controller"=GetSeatVisitsAction::class,
 *          },
 *          "delete"
 *     },
 *     attributes={"pagination_enabled"=false}
 * )
 *
 * @ORM\Table(name="seats")
 * @ORM\Entity()
 */
class Seat
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=120)
     * @ORM\Column(name="name", type="string", length=128)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="Visit", mappedBy="seat")
     */
    protected $visits;

    public function __construct()
    {
        $this->visits = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getVisits()
    {
        return $this->visits;
    }

    /**
     * @param ArrayCollection $visits
     */
    public function setVisits($visits): void
    {
        $this->visits = $visits;
    }
}
