<?php

namespace App\Entity\Repository;

use App\Entity\Visit;
use Doctrine\ORM\EntityRepository;

class VisitRepository extends EntityRepository
{
    public function findVisitsBetweenDatesBySeatQueryBuilder(\DateTime $startsAt, \DateTime $endsAt, Visit $visit = null)
    {
        $qb = $this->createQueryBuilder('v')
            ->select('v')

            ->where('v.startsAt < :endsAt')
            ->setParameter('endsAt', $endsAt)

            ->andWhere('v.endsAt > :startsAt')
            ->setParameter('startsAt', $startsAt)
        ;


        if( $visit && $visit->getId() )
        {
            $qb
                ->andWhere('v.id != :id')
                ->setParameter('id', $visit->getId());
        }

        return $qb;

    }
}