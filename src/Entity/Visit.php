<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Validator\UniquePeriodSeatVisit;
use Carbon\Carbon;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "get"={
 *          },
 *          "post"={
 *          }
 *     },
 *     itemOperations={
 *          "get"={
 *          },
 *          "put" = {
 *          },
 *          "delete"
 *     },
 *     attributes={
 *          "pagination" = false,
 *     }
 * )
 *
 * @ORM\Table(name="visits")
 * @ORM\Entity(repositoryClass="App\Entity\Repository\VisitRepository")
 *
 * @UniquePeriodSeatVisit()
 */
class Visit
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="starts_at", type="datetime")
     */
    protected $startsAt;

    /**
     * @Assert\NotBlank()
     * @ORM\Column(name="ends_at", type="datetime")
     */
    protected $endsAt;

    /**
     * Groups({"PLANT:OUT", "PLANT:INPUT"})
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="Seat")
     * @ORM\JoinColumn(name="seat", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $seat;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStartsAt()
    {
        return $this->startsAt;
    }

    /**
     * @param mixed $startsAt
     */
    public function setStartsAt($startsAt): void
    {
        $this->startsAt = $startsAt;
    }

    /**
     * @return mixed
     */
    public function getEndsAt()
    {
        return $this->endsAt;
    }

    /**
     * @param mixed $endsAt
     */
    public function setEndsAt($endsAt): void
    {
        $this->endsAt = $endsAt;
    }

    /**
     * @return mixed
     */
    public function getSeat()
    {
        return $this->seat;
    }

    /**
     * @param mixed $seat
     */
    public function setSeat($seat): void
    {
        $this->seat = $seat;
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if($this->startsAt instanceof \DateTime && $this->endsAt instanceof \DateTime)
        {
            if(Carbon::instance($this->startsAt)->gte( Carbon::instance($this->endsAt) ))
            {
                $context
                    ->buildViolation('Ends earlier that its starts.')
                    ->atPath('endsAt')
                    ->addViolation();
            }

        }
    }
}
