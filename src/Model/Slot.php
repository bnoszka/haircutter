<?php

namespace App\Model;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\GetDaySlotsAction;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     description="List slot per day.",
 *     collectionOperations={
 *          "get"={
 *              "method"="GET",
 *              "path"="/slots-per-day/{id}",
 *              "controller"=GetDaySlotsAction::class,
*               "normalization_context"={"groups"={"out"}},
 *          },
 *      },
 *     itemOperations={"get"},
 *     attributes={"pagination_enabled"=false}
 * )
 */
class Slot
{
    /**
     * @ApiProperty(identifier=true)
     * @Groups("out")
     */
    protected $id;

    /**
     * @Groups("out")
     */
    protected $startsAt;

    /**
     * @Groups("out")
     */
    protected $endsAt;
    protected $visit;

    public function __construct($id, $startsAt, $endsAt)
    {
        $this->id = $id;
        $this->startsAt = $startsAt;
        $this->endsAt = $endsAt;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getStartsAt()
    {
        return $this->startsAt;
    }

    public function setStartsAt($startsAt): void
    {
        $this->startsAt = $startsAt;
    }

    public function getEndsAt()
    {
        return $this->endsAt;
    }

    public function setEndsAt($endsAt): void
    {
        $this->endsAt = $endsAt;
    }

    public function getVisit()
    {
        return $this->visit;
    }

    public function setVisit($visit): void
    {
        $this->visit = $visit;
    }
}